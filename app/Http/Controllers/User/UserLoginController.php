<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Reminder;
use Sentinel;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use App\User;
use DB;
use View;

class UserLoginController extends Controller
{
   public function showLogin(){
       if (Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug != 'Admin')
           return redirect('/userdashboard')->with(['error'=>'Almost Logged In']);
       else
       return view('user.login');
   }

   public function showRegister(){
       if (Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug != 'Admin')
           return redirect('/userdashboard')->with(['error'=>'Almost Logged In']);
       else
       return view('user.register');
   }

   public function userDashboard(){

       if (Sentinel::getUser()->roles()->first()->slug === 'Medico_Legal_Agency')
       {$id = Sentinel::getUser()->id;
           $thisDay = DB::table('appointments')
               ->where('agency_id',$id)
               ->whereDate('created_at', DB::raw('CURDATE()'))->count();
           $fromDate = date('Y-m-d',strtotime('-7 day'));
           $tillDate = date('Y-m-d');
           $thisWeek = DB::table('appointments')
               ->where('agency_id',$id)
               ->whereBetween('created_at', [$fromDate,$tillDate])
               ->count();
           $thisMonth = DB::table('appointments')
               ->where('agency_id',$id)
               ->whereMonth('created_at', date('m'))
               ->whereYear('created_at',date('Y'))
               ->count();
           $agency = DB::table('enlisteds')
               ->join('users', 'users.id', '=', 'enlisteds.experts_id')
               ->where('enlisteds.agency_id', $id)
               ->select('users.*')
               ->count();

       return View::make('user.user-dashboard')
           ->with('thisDay',$thisDay)
           ->with('thisWeek',$thisWeek)
           ->with('thisMonth',$thisMonth)
           ->with('agency',$agency);
       }
       elseif(Sentinel::getUser()->roles()->first()->slug === 'Medico_Legal_Expert')
       {
           $id = Sentinel::getUser()->id;
           $thatDay = DB::table('appointments')
               ->where('experts_id',$id)
               ->whereDate('created_at', DB::raw('CURDATE()'))->count();
           $fromDate = date('Y-m-d',strtotime('-7 day'));
           $tillDate = date('Y-m-d');
           $thatWeek = DB::table('appointments')
               ->where('experts_id',$id)
               ->whereBetween('created_at', [$fromDate,$tillDate])
               ->count();
           $thatMonth = DB::table('appointments')
               ->where('experts_id',$id)
               ->whereMonth('created_at', date('m'))
               ->whereYear('created_at',date('Y'))
               ->count();
           $doctor = DB::table('enlisteds')
               ->join('users', 'users.id', '=', 'enlisteds.agency_id')
               ->where('enlisteds.experts_id', $id)
               ->select('users.*')
               ->count();
           return view('user.user-dashboard')
               ->with('thatDay',$thatDay)
               ->with('thatWeek',$thatWeek)
               ->with('thatMonth',$thatMonth)
               ->with('doctor',$doctor);
       }

   }

    public function postLogin(Request $request)
    {
        $request->validate([
            'email'  => 'required',
            'password'=>'required'
        ]);
        $user = User::where('email', '=', $request->email)->first();
        if ($user === null) {
            return redirect()->back()->with(['error'=>'Wrong login information']);
        }
        try {
            $rememberMe = false;
            if(isset($request->remember_me))
                $rememberMe = true;
            if (Sentinel::authenticate($request->all(), $rememberMe)) {
                if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug === 'Admin')
                    return redirect()->back()->with(['error'=>'Wrong login information']);
                else
                    return redirect('/userdashboard')->with(['success'=>'Welcome to the dashboard']);
            } else {
                return redirect()->back()->with(['error' => 'Wrong login information']);
            }
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            return redirect()->back()->with(['error' => "You are band for $delay seconds"]);

        } catch (NotActivatedException $e) {

            return redirect()->back()->with(['error' => 'Not Activated Yet']);

        }

    }
    public function postLogout(){
        Sentinel::logout();
        return redirect('/show-login')->with(['success'=>'Logged out Successfully']);
    }

    public function forgotPassword(Request $request)
    {
        $admin = Sentinel::findByCredentials(['email' => $request->email]);

        if (count($admin) === 1) {
            $reminder = Reminder::exists($admin) ?: Reminder::create($admin);
            $this->_sendEmail($admin, $reminder->code);
        }

        return redirect()->back()->with(['success' => 'Reset code was sent to your email']);
    }

    public function showResetPassword(Request $request) {
        $admin = Sentinel::findById($request->id);

        if (!$admin) {
            abort(404);
        }

        if(($reminder = Reminder::exists($admin)) && $request->code === $reminder->code) {
            return view('user.resset-password')->with(['id' => $request->id, 'code' => $request->code]);
        }

        return redirect('/show-login');
    }

    private function _sendEmail($user , $code)
    {
        Mail::send('email.password-reset', [
            'id' => $user->id,
            'code' => $code
        ], function($message) use ($user) {
            $message->to($user->email);
            $message->subject("Hello $user->name, reset your password");
        });
    }

    public function postResetPassword(Request $request, $id, $resetCode)
    {
        $request->validate([
            'password'              => 'required|min:8',
            'password_confirmation' => 'same:password'
        ]);

        $user = Sentinel::findById($id);

        if (!$user) {
            abort(404);
        }

        if(($reminder = Reminder::exists($user)) && $resetCode === $reminder->code) {
            Reminder::complete($user, $resetCode , $request->password);
            return redirect('/show-login')->with(['success' => 'Please log in with your new password']);
        }

        return redirect('/show-login')->with(['error' => 'Something went wrong!']);
    }


}
