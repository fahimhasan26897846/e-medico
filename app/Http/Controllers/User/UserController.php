<?php

namespace App\Http\Controllers\User;

use App\Appointment;
use App\Enlisted;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use View;
use Sentinel;
use DB;

class UserController extends Controller
{
    public function manageExpert($id){

        $flights = DB::table('enlisteds')
            ->join('users', 'users.id', '=', 'enlisteds.experts_id')
            ->where('enlisteds.agency_id', $id)
            ->select('users.*')
            ->get();
        $other = DB::table('users')
            ->where('users.type','Medico_Legal_Expert')
            ->select('users.*')
            ->get();

       return View::make('user.manage-experts')
           ->with('users',$flights)
           ->with('others',$other);
    }

    public function addExpert(Request $request){
//        $check =Enlisted::where('experts_id', '=', $request->experts_id)->first();
//        if($check !=null)
//        {
//            return redirect()->back()->with(['error'=>'Expert is almost in your list']);
//        }
        $obj = new Enlisted();
        $obj->agency_id = $request->agency_id;
        $obj->experts_id = $request->experts_id;
        $result =$obj->save();
        if ($result)
            return redirect()->back()->with(['success'=>'Expert Has Been Add To your List']);
        else
            return redirect()->back()->with(['error'=>'Opps! Something went wrong. PLease try again after few minute letter']);
    }

    public function deleteExpert($id){
        $currentUser = Sentinel::getUser()->id;
         $obj = DB::table('enlisteds')
            ->where('enlisteds.agency_id',$currentUser)
            ->where('enlisteds.experts_id',$id)
            ->delete();
        if ($obj)
            return redirect()->back()->with(['success'=>'Expert Removed From your List']);
        else
            return redirect()->back()->with(['error'=>'Opps! Something went wrong. PLease try again after few minute letter']);

    }

    public function searchSlot(){
        return view('user.bookappointment');
    }

    public function findSlot(Request $request){
        if ($request->ajax()){
            $output = "";
            $fuck = "Fuck you";
            $experts = DB::table('users')
                ->where('type', 'Medico_Legal_Expert')
                ->where('post_code','LIKE','%'.$request->search.'%')
                ->get();
            if ($experts){

                foreach ($experts as $key=>$expert){
                    $output.='<tr>'.
                        '<td>'.$expert->id.'</td>'.
                        '<td>'.$expert->name.'</td>'.
                        '<td>'.$expert->post_code.'</td>'.
                        '<td>'.$expert->max_slot.'</td>'.
                        '<td>'.'<form action="/book-appointment" method="post">'. csrf_field() .'<input type="hidden" name="agency_id" value='.Sentinel::getUser()->id.'><input type="hidden" name="agency_name" value='.Sentinel::getUser()->name.'><input type="hidden" name="expert_id" value='.$expert->id.'><input type="hidden" name="expert_name" value='.$expert->name.'><input type="hidden" name="location_one" value='.$expert->address_line1.'><input type="hidden" name="location_two" value='.$expert->address_line2.'><button class="btn btn-sm btn-primary">Book Appointment</button></form>'.'</td>'.
                        '</tr>';
                }

                return response($output);
            }

        }

    }

    public function createAppointment(Request $request){
        return View::make('user.appointment')
            ->with('agency_id',$request->agency_id)
            ->with('agency_name',$request->agency_name)
            ->with('expert_id',$request->expert_id)
            ->with('expert_name',$request->expert_name)
            ->with('location_one',$request->location_one)
            ->with('location_two',$request->location_two);

    }

    public function postCreateAppointment(Request $request){
       $request->validate([
          'doctor_id'   => 'required',
          'doctor_name' => 'required',
          'agency_id'   => 'required',
          'agency_name' => 'required',
           'location'   => 'required',
           'client_name'=> 'required',
           'date'       => 'required',
           'time'       => 'required',
           'dob'        => 'required',
       ]);
       $obj = new Appointment();
       $obj->experts_id = $request->doctor_id;
       $obj->expert_name = $request->doctor_name;
       $obj->agency_id = $request->agency_id;
       $obj->agency_reference = $request->agency_name;
       $obj->client = $request->client_name;
       $obj->dob = $request->dob;
       $obj->appointment_date = $request->date;
       $obj->appointment_time = $request->time;
       $obj->appointment_place = $request->location;
       $result = $obj->save();
       if ($result)
           return redirect('/userdashboard')->with(['success'=>'Appointment Created Successfully']);
       else
           return redirect('/userdashboard')->with(['error'=>'Opps! Something went wrong. Please try again letter']);

    }

    public function todaysAgencyAppointments($id){
        $today = DB::table('appointments')
            ->where('agency_id',$id)
            ->whereDate('created_at', DB::raw('CURDATE()'))->get();
        return View::make('user.agent-todays-appointment')
            ->with('todays',$today);
    }

    public function thisWeekAgencyAppointments($id){
        $fromDate = date('Y-m-d',strtotime('-7 day'));
        $tillDate = date('Y-m-d');
        $today = DB::table('appointments')
            ->where('agency_id',$id)
            ->whereBetween('created_at', [$fromDate,$tillDate])
            ->get();

        return View::make('user.agent-weeks-appointment')
            ->with('todays',$today);
    }

    public function thisMonthAgencyAppointments($id){
        $today = DB::table('appointments')
            ->where('agency_id',$id)
            ->whereMonth('created_at', date('m'))
            ->whereYear('created_at',date('Y'))
            ->get();

        return View::make('user.agent-months-appointment')
            ->with('todays',$today);
    }

    public function todaysExpertAppointments($id){
        $today = DB::table('appointments')
            ->where('experts_id',$id)
            ->whereDate('created_at', DB::raw('CURDATE()'))->get();
        return View::make('user.experts-todays-appointment')
            ->with('todays',$today);
    }

    public function thisWeekExpertAppointments($id){
        $fromDate = date('Y-m-d',strtotime('-7 day'));
        $tillDate = date('Y-m-d');
        $today = DB::table('appointments')
            ->where('experts_id',$id)
            ->whereBetween('created_at', [$fromDate,$tillDate])
            ->get();

        return View::make('user.experts-weeks-appointment')
            ->with('todays',$today);
    }

    public function thisMonthExpertAppointments($id){
        $today = DB::table('appointments')
            ->where('experts_id',$id)
            ->whereMonth('created_at', date('m'))
            ->whereYear('created_at',date('Y'))
            ->get();

        return View::make('user.experts-months-appointment')
            ->with('todays',$today);
    }
    public function deleteAppointment($id){
        $obj = new Appointment();
        $search = $obj->find($id);
        $result = $search->delete();
        if ($result)
            return redirect('/userdashboard')->with(['success'=>'Appointment deleted Successfully']);
        else
            return redirect('/userdashboard')->with(['error'=>'Opps! Something went wrong. Please try again letter']);
    }

    public function getAgencies($id){
        $agencies = DB::table('enlisteds')
            ->join('users', 'users.id', '=', 'enlisteds.agency_id')
            ->where('enlisteds.experts_id', $id)
            ->select('users.*')
            ->get();
        return View::make('user.agency-list')
            ->with('agencies',$agencies);

    }


}
