<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;

class UserSettingsController extends Controller
{
    public function showSettings(){
        return view('user.settings');
    }

    public function updateGenerelSettings(Request $request){
        $request->validate([
            'name' => 'required',
            'contact' => 'required',
        ]);

        $obj = new User();
        $content = $obj->find($request->id);
        $content->name = $request->name;
        $content->contact = $request->contact;
        $result =$content->update();
        if ($result)
            return redirect()->back()->with(['success'=>'Profile Has Been Updated']);
        else
           return redirect()->back()->with(['error'=>'Something went wrong please try again letter']);
    }

    public function updateSettings(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required|min:10',
            'confirm_password' =>'same:password'
        ]);
        $user = Sentinel::getUser();
       $result= Sentinel::update($user, array('password' => $request->password));
       if($result)
           return redirect()->back()->with(['success'=>'info Updated']);
       else
           return redirect()->back()->with(['error'=>'Something went wrong. Please try again letter']);
    }

    public function updateInfoSettings(Request $request){
        $request->validate([
            'cv' => 'file|max:5000',
            'address_line_one' => 'required',
            'gmc' =>'required',
            'speciality' => 'required',
            'qualification' => 'required',
            'appointment_price' => 'required',
            'max_slot'  => 'required'
        ]);
        $obj = new User();
        $object = $obj->find($request->id);
        $object->building_no = $request->building_no;
        $object->building_name = $request->building_name;
        $object->city = $request->city;
        $object->post_code = $request->post_code;
        $object->address_line1 = $request->address_line_one;
        $object->address_line2 = $request->address_line_two;
        $object->gmc = $request->gmc;
        $object->dna = $request->dna;
        $object->speciality = $request->speciality;
        $object->qualification = $request->qualification;
        $object->max_slot = $request->max_slot;
        $object->appointment_price = $request->appointment_price;
        $object->returning_fees = $request->returning_fees;
        $objFile =    $request->file('cv');
        if($objFile !=Null){
            $fileName=  time().$objFile->getClientOriginalName();
            $object->cv =$fileName;
            @mkdir('CV');
            $destinationPath = 'CV';
            $objFile->move($destinationPath,$fileName);
        }
        $status= $object->update();
        if ($status)
            return redirect()->back()->with(['success'=>'Profile Updated Successfully']);
        else
            return redirect()->back()->with(['error'=>'Something Went Wrong. Try Again Letter']);
    }
}
