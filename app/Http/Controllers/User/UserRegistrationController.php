<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Activation;
use Mail;

class UserRegistrationController extends Controller
{
    public function postRegistration(Request $request)
    {
        $request->validate([
            'name'           => 'required',
            'type'               =>'required',
            'gmc'                => 'required',
            'contact'            =>'required',
            'email'              => 'required|email',
            'password'           => 'required|min:8',
            'confirm_password'   => 'same:password',
            'agree'              => 'accepted'
        ]);

        if (Sentinel::findByCredentials(['email' => $request->email])) {
            return redirect()->back()->with(['error' => 'Email already exists']);
        }

        $user = Sentinel::register($request->all());
        $activation = Activation::create($user);
        $role = Sentinel::findRoleBySlug($request->type);
        $role->users()->attach($user);
        $this->_sendEmail($user, $activation->code);

        return redirect('/show-login')->with(['success' => 'Please check your email for account activation']);
    }

    private function _sendEmail($user, $code)
    {
        Mail::send('email.activation', [
            'id'    => $user->id,
            'code'  => $code
        ], function ($message) use($user) {
            $message->to($user->email);
            $message->subject("Hello {$user->name}, Activate your account");
        });
    }

    public function activateUser(Request $request)
    {
        $user = Sentinel::findById($request->id);

        if (Activation::completed($user)) {
            return redirect('/show-login')->with(['success' => 'Your account is already activated. Please log in.']);
        }

        if (Activation::complete($user, $request->code)) {
            return redirect('/show-login')->with(['success' => 'Your account has been activated. Please log in.']);
        }

        return redirect('/show-register')->with(['error' => 'Something went wrong ! Please register again.']);
    }
}
