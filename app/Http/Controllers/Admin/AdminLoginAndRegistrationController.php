<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Mail;
use Activation;

class AdminLoginAndRegistrationController extends Controller
{
   public function showRegistration(){
       return view('admin.create-admin');
   }

   public function postRegistration(Request $request)
    {
        $request->validate([
            'name'               => 'required',
            'contact'            =>'required',
            'email'              => 'required|email',
            'password'           => 'required|min:8',
            'confirm_password'   => 'same:password',
        ]);

        if (Sentinel::findByCredentials(['email' => $request->email])) {
            return redirect()->back()->with(['error' => 'Email already exists']);
        }

        $user = Sentinel::registerAndActivate($request->all());
        $role = Sentinel::findRoleBySlug('Admin');
        $role->users()->attach($user);

        return redirect('/admin-list')->with(['success' => 'New Admin has been created']);
    }
}
