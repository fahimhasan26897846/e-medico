<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sentinel;
use Mail;

class AdminLoginController extends Controller
{
    public  function showLogin(){
        if (Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug === 'Admin')
            return redirect('/dashboard')->with(['error'=>'Almost Logged In']);
        else
        return view('admin.admin-login');
    }
    public function postLogin(Request $request)
    {
        $request->validate([
            'email'  => 'required',
            'password'=>'required'
        ]);
        $user = User::where('email', '=', $request->email)->first();
        if ($user === null) {
            return redirect()->back()->with(['error'=>'Wrong login information']);
        }
        try {
            $rememberMe = false;
            if(isset($request->remember_me))
                $rememberMe = true;
            if (Sentinel::authenticate($request->all(), $rememberMe)) {
                if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug != 'Admin')
                    return redirect()->back()->with(['error'=>'Wrong login information']);
                else
                    return redirect('/dashboard')->with(['success'=>'Welcome to the dashboard']);
            } else {
                return redirect()->back()->with(['error' => 'Wrong login information']);
            }
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            return redirect()->back()->with(['error' => "You are band for $delay seconds"]);

        }

    }
    public function postLogout(){
        Sentinel::logout();
        return redirect('/admin-login')->with(['success'=>'Logged out Successfully']);
    }

    public function forgotPassword(Request $request)
    {
        $admin = Sentinel::findByCredentials(['email' => $request->email]);

        if (count($admin) === 1) {
            $reminder = Reminder::exists($admin) ?: Reminder::create($admin);
            $this->_sendEmail($admin, $reminder->code);
        }

        return redirect()->back()->with(['success' => 'Reset code was sent to your email']);
    }

    public function showResetPassword(Request $request) {
        $admin = Sentinel::findById($request->id);

        if (!$admin) {
            abort(404);
        }

        if(($reminder = Reminder::exists($admin)) && $request->code === $reminder->code) {
            return view('user.resset-password')->with(['id' => $request->id, 'code' => $request->code]);
        }

        return redirect('/show-login');
    }

    private function _sendEmail($user , $code)
    {
        Mail::send('email.password-reset', [
            'id' => $user->id,
            'code' => $code
        ], function($message) use ($user) {
            $message->to($user->email);
            $message->subject("Hello $user->name, reset your password");
        });
    }

    public function postResetPassword(Request $request, $id, $resetCode)
    {
        $request->validate([
            'password'              => 'required|min:8',
            'password_confirmation' => 'same:password'
        ]);

        $user = Sentinel::findById($id);

        if (!$user) {
            abort(404);
        }

        if(($reminder = Reminder::exists($user)) && $resetCode === $reminder->code) {
            Reminder::complete($user, $resetCode , $request->password);
            return redirect('/show-login')->with(['success' => 'Please log in with your new password']);
        }

        return redirect('/show-login')->with(['error' => 'Something went wrong!']);
    }
}
