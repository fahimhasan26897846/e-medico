<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Faq;
use App\NewsLetter;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use DB;
use Mail;
use Sentinel;


class AdminController extends Controller
{
    public function showDashboard(){
        $experts = DB::table('users')
            ->where('type','Medico_Legal_Expert')
            ->count();
        $agency = DB::table('users')
            ->where('type','Medico_Legal_Agency')
            ->count();
        $appointment = DB::table('appointments')->count();
        return View::make('admin.dashboard')
            ->with('experts',$experts)
            ->with('agency',$agency)
            ->with('appointments',$appointment);
    }

    public function showFaq(){


        return View::make('admin.faq')
            ->with('faqs',Faq::all());
    }

    public function createFaq(Request $request){
        $request->validate([
           'question' => 'required',
           'answer'   => 'required'
        ]);

        $faq = new Faq();
        $faq->questions = $request->question;
        $faq->answer = $request->answer;
        $result = $faq->save();
        if ($result)
            return redirect()->back()->with(['success'=>'New faq has been created successfully']);
        else
            return redirect()->back()->with(['error'=>'Opps! Something went wrong pleasetry again within few minute letter']);

    }

    public function updateFaq(Request $request){
        $request->validate([
            'question' => 'required',
            'answer'   => 'required'
        ]);

        $update = new Faq();
        $updateFaq = $update->find($request->id);
        $updateFaq->questions = $request->question;
        $updateFaq->answer = $request->answer;
        $result =$updateFaq->update();
        if ($result)
            return redirect()->back()->with(['success'=>'New faq has updated successfully']);
        else
            return redirect()->back()->with(['error'=>'Opps! Something went wrong pleasetry again within few minute letter']);
    }

    public function showNewsLetter(){
        return View::make('admin.newsletters')
            ->with('emails',NewsLetter::all());
    }

    public function postNewsLetter(Request $request){

        $request->validate([
           'subject' => 'required',
           'message' => 'required'
        ]);

        $titles = DB::table('news_letters')->pluck('email');
        foreach ($titles as $title) {
            $data = array(
                'email'=> $title,
                'subject' => $request->subject,
                'bodyMessage' => $request->message

            );
            Mail::send('email.newsletter', $data, function ($message) use ($data) {
                $message->to($data['email']);
                $message->subject($data['subject']);
            });
        }
        return redirect()->back()->with(['success' =>'All newsletter has been sent successfully']);
    }

    public function deleteNewsLetter($id){
        $obj = new NewsLetter();
        $delete = $obj->find($id);
        $result = $delete->delete();
        if ($result)
            return redirect()->back()->with(['success'=>'Newsletter has deleted successfully']);
        else
            return redirect()->back()->with(['error'=>'Opps! Something went wrong pleasetry again within few minute letter']);

    }

    public function adminContact(){
        return View::make('admin.contact-admin')
            ->with('contacts',Contact::all()->sortByDesc("id"));
    }

    public function deleteContactMessage($id){
        $obj = new Contact();
        $contact = $obj->find($id);
        $result = $contact->delete();
        if ($result)
            return redirect()->back()->with(['success'=>'Message has deleted successfully']);
        else
            return redirect()->back()->with(['error'=>'Opps! Something went wrong pleasetry again within few minute letter']);

    }

    public function replyContact(Request $request){
        $data = array(
            'email' => $request->email,
            'subject' => 'CIRCO HEALTH CARE',
            'bodyMessage' => $request->message

        );

        Mail::send('admin.email.reply',$data,function ($message) use ($data){
            $message->to($data['email']);
            $message->subject($data['subject']);
        });


            return redirect()->back()->with(['success'=>'Message has sent successfully']);

    }

    public function showAdmin(){
        $value= DB::table('users')
            ->join('role_users', 'users.id', '=', 'role_users.user_id')
            ->join('roles', 'roles.id', '=', 'role_users.role_id')
            ->where('roles.slug', 'Admin')
            ->select('users.*')
            ->get();
        return View::make('admin.admin-list')
            ->with('admins',$value);

    }

    public function deleteAdmin($id){
        if($id === Sentinel::getUser()->id)
            return redirect()->back()->with(['error'=>'You can not delete yourself']);
        else{
            $obj = new User();
            $object = $obj->find($id);
            $result = $object->delete();
            if ($result)
                return redirect()->back()->with(['success'=>'Admin Deleted']);
            else
                return redirect()->back()->with(['error'=>'Opps! Something went wrong pleasetry again within few minute letter']);
        }



    }


}
