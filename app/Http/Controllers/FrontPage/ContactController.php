<?php

namespace App\Http\Controllers\FrontPage;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function postMessage(Request $request){
        $request->validate([
            'name'         => 'required',
            'email'          => 'required|email',
            'phone'              => 'required|numeric',
            'message'           => 'required',
        ]);

        $contact = new Contact();
        $contact->name = $request->name;
        $contact->contact = $request->phone;
        $contact->email = $request->email;
        $contact->message = $request->message;
       $result= $contact->save();
       if ($result)
           return redirect()->back()->with(['success'=>'Message Has Been Sent Successfully']);
       else
           return redirect()->back()->with(['error'=>'Your Message was not Sent. Try Again After Few minute']);
    }
}
