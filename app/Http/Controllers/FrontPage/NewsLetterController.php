<?php

namespace App\Http\Controllers\FrontPage;

use App\NewsLetter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

class NewsLetterController extends Controller
{
    public function postSubscription(Request $request){

        $check = NewsLetter::where('email', '=', $request->email)->first();
        if ($check != null) {
            return redirect()->back()->with(['error'=>'Aww, You Have Almost Subscribe This Website']);
        }

        $request->validate([
            'email' => 'required|email'
        ]);

        $newsLetter = new NewsLetter();
        $newsLetter->email = $request->email;
        $result =$newsLetter->save();
        if ($result){
            $data = array(
                'email' =>$request->email
            );
            Mail::send('email.thanks',$data,function ($message) use ($data){
                $message->to($data['email']);
                $message->subject('Thanks For Subscribing Our Website');
            });
            return redirect()->back()->with(['success'=>'Thanks For Subscribing Our Website. We will start send Our Notification Soon']);}
        else
            return redirect()->back()->with(['error'=>'Sorry Something Went Wrong']);
    }

    private function sendEmail($user,$code){
        Mail::send('SuperAdmin.emails.activation',[
            'user' => $user,
            'code' => $code
        ], function ($message) use ($user){
            $message->to($user->email);
            $message->subject("Hello $user->first_name, Activate your account");
        } );
    }
}
