<?php

namespace App\Http\Controllers\FrontPage;

use App\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class FrontPageController extends Controller
{
    public function mainPage(){
        return view('front.index');
    }

    public function aboutPage(){
        return view('front.about');
    }

    public function contactPage(){
        return view('front.contact');
    }

    public function pricingPage(){
        return view('front.pricing');
    }

    public function faqPage(){
        return View::make('front.faq')
            ->with('faqs',Faq::all());;
    }

}
