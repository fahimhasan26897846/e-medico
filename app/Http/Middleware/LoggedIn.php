<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class LoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug != 'Admin'){
            return $next($request);
        }
        else
            return redirect('/show-login')->with(['error'=>'Log In First']);

    }
}

/*
 * && Sentinel::getUser()->roles()->first()->slug != 'Admin' && Sentinel::getUser()->roles()->first()->slug === 'Medico_Legal_Expert' || Sentinel::getUser()->roles()->first()->slug === 'Medico_Legal_Agency' || Sentinel::getUser()->roles()->first()->slug === 'Solicitor'
 * */
