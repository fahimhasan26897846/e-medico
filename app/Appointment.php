<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $fillable = ['experts_id','expert_name','agency_id','client','agency_reference','dob','date','time','appointment_place'];
}
