<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enlisted extends Model
{
    protected $fillable = ['experts_id','agency_id'];
}
