<?php

use Illuminate\Database\Seeder;

class ActivationSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'user_id' => '1','code' =>'ODvB688jrQD0FLal03IvILTkTYy8rbNK','completed'=>'1','completed_at'=>'2018-02-13 18:43:51'],
            ['id' => 2, 'user_id' => '2','code' =>'FHlmlGZg28b4bEbOxzcrVxs0BcOcEghS','completed'=>'1','completed_at'=>'2018-02-16 18:10:07'],
            ['id' => 3, 'user_id' => '3','code' =>'JVRKtnDj2oKJWNEEKC9yuyzDIylkeKwx','completed'=>'1','completed_at'=>'2018-02-16 18:15:46'],


        ];

        foreach ($items as $item) {
            \App\Activation::create($item);
        }
    }
}
