<?php

use Illuminate\Database\Seeder;

class RoleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'slug' => 'Admin','name' =>'Admin'],
            ['id' => 2, 'slug' => 'Medico_Legal_Expert', 'name' =>'Medico Legal Expert'],
            ['id' => 3, 'slug' => 'Medico_Legal_Agency','name' =>'Medico Legal Agency'],
            ['id' => 4, 'slug' => 'Solicitor', 'name' =>'Solicitor']

        ];

        foreach ($items as $item) {
            \App\Role::create($item);
        }
    }
}
