<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['id' => 1, 'email' => 'fahimhasan202020@gmail.com','name' =>'Fahim Hasan','type'=>'Medico_Legal_Expert','password'=>'$2y$10$9CxN3Qu3U/YeD7TPNO5tNOCeEw6/X.2N8BhsGetvqqjtR0GPhhysa','active'=>'1','contact'=>'01676081282'],
            ['id' => 2, 'email' => 'dreamtechnology2050@gmail.com','name' =>'Fahim Hasan','type'=>'Medico_Legal_Agency','password'=>'$2y$10$BkMJmr1w1P//FwSi2MC92esUZBtLCkRq.8W8ZZP.lt6Pwifzq2sym','active'=>'1','contact'=>'01676081282'],
            ['id' => 3, 'email' => 'demo@x692222.com','name' =>'fahim','type'=>'','password'=>'$2y$10$pREhuxUQbAGZ4QQumSWI1ew9tmEnMUV261kIIroFplumPp61dTpYu','active'=>'1','contact'=>'01676081282'],
        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
