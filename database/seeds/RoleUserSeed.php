<?php

use Illuminate\Database\Seeder;

class RoleUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            ['user_id' => '1','role_id' =>'2'],
            ['user_id' => '2','role_id' =>'3'],
            ['user_id' => '3','role_id' =>'1'],
        ];

        foreach ($items as $item) {
            \App\RoleUser::create($item);
        }
    }
}
