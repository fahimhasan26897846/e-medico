<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOthersTableDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_letters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('contact')->nullable();
            $table->string('email');
            $table->string('message',500);
            $table->timestamps();

            $table->engine = 'InnoDB';
        });

        Schema::create('faqs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('questions',300);
            $table->string('answer',300);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('experts_id',300);
            $table->string('agency_id',300);
            $table->string('expert_name',500);
            $table->string('appointment_time',500);
            $table->string('client',500);
            $table->string('agency_reference',200);
            $table->string('dob',200);
            $table->string('appointment_date',200);
            $table->string('appointment_place',500);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });

        Schema::create('enlisteds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('experts_id',300);
            $table->string('agency_id',300);
            $table->timestamps();
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_letters');
        Schema::dropIfExists('contacts');
        Schema::dropIfExists('faqs');
        Schema::dropIfExists('appointments');
        Schema::dropIfExists('enlisteds');
    }
}
