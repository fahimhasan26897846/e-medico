@extends('front.inc.master')
@section('title','Error 403')
@section('style')
    <link rel="stylesheet" href="{{asset('front-page-resource/css/errors.css')}}">
    @endsection
@php
    $active = 'home'
@endphp
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <section id="wrapper" class="error-page">
        <div class="error-box">
            <div class="error-body text-center">
                <h1>403</h1>
                <h3 class="text-uppercase">Forbidden Error</h3>
                <p class="text-muted m-t-30 m-b-30 text-uppercase">You don't have permission to access on this server.</p>
                <a href="/" class="btn btn-info btn-rounded waves-effect">Back to home</a> </div>
            <footer class="footer text-center">2017 © Elite Admin.</footer>
        </div>
    </section>
@endsection