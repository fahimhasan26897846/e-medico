@extends('front.inc.master')
@section('title','Error 404')
@section('style')
    <link rel="stylesheet" href="{{asset('front-page-resource/css/errors.css')}}">
@endsection
@php
    $active = 'home'
@endphp
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <section id="wrapper" class="error-page">
        <div class="error-box">
            <div class="error-body text-center">
                <h1>404</h1>
                <h3 class="text-uppercase">Page Not Found !</h3>
                <p class="text-muted m-t-30 m-b-30">YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
                <a href="/" class="btn btn-info btn-lg btn-rounded waves-effect">Back to home</a> </div>
            <footer class="footer text-center">2017 © E-medico.</footer>
        </div>
    </section>
    @endsection