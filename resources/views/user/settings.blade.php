@extends('front.inc.master')
@section('title','Dashboard')
@php
    $active = 'dashboard';
    $sideNav = 'settings'
@endphp
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-2">
                @include('user.inc.sidenav')
            </div>
            <div class="col-md-10">
                <h2 class="bg-primary" style="padding: 20px;border-radius: 5px;margin-bottom: 10px">Settings</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div style="border-radius: 10px;border: 2px solid rgba(82,82,63,0.32);margin-bottom: 10px"><h3 class="bg-info" style="padding: 10px">Generel Setting</h3>
                            <div style="margin: 10px;padding: 10px">
                                <form action="/updategenerelsettings" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{Sentinel::getUser()->id}}">
                                    <div class="form-group">
                                        <label for="name">Name</label>
                                        <input type="text" name="name" value="{{Sentinel::getUser()->name}}" class="form-control">
                                    </div>
                                    @if ($errors->has('name'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('name')}}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="contact">Contact</label>
                                        <input type="number" name="contact" value="{{Sentinel::getUser()->contact}}" class="form-control">
                                    </div>
                                    @if ($errors->has('contact'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('contact')}}</p>
                                    @endif
                                    <button type="submit" class="btn btn-info btn-block"><i class="fa fa-sign-in"></i>&nbsp; &nbsp; Save</button>
                                </form>
                            </div>
                        </div>

                        <div style="border-radius: 10px;border: 2px solid rgba(82,82,63,0.32);margin-bottom: 10px"><h3 class="bg-info" style="padding: 10px">Login Detail</h3>
                            <div style="margin: 10px;padding: 10px">
                                <form action="/updatesettings" method="post" >
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{Sentinel::getUser()->id}}">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" value="{{Sentinel::getUser()->email}}" class="form-control" readonly>
                                    </div>
                                    @if ($errors->has('email'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="password">New Password</label>
                                        <input type="password" name="password" placeholder="New password" class="form-control">
                                    </div>
                                    @if ($errors->has('password'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="confirm password">Confirm Password</label>
                                        <input type="password" name="confirm_password" placeholder="Confirm New Password" class="form-control">
                                    </div>
                                    @if ($errors->has('confirm_password'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('confirm_password')}}</p>
                                    @endif
                                    <button type="submit" class="btn btn-info btn-block"><i class="fa fa-sign-in"></i>&nbsp; Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"><div style="border-radius: 10px;border: 2px solid rgba(82,82,63,0.32);margin-bottom: 10px"><h3 class="bg-info" style="padding: 10px">Personal Info</h3>
                            <div style="margin: 10px;padding: 10px">
                                <form action="/updatepersonalinfo" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{Sentinel::getUser()->id}}">
                                    <h4>Address</h4>
                                    <hr>
                                    <div class="form-group">
                                        <label for="building no">Building No</label>
                                        <input type="number" name="building_no" class="form-control" value="{{Sentinel::getUser()->building_no}}">
                                    </div>
                                    @if ($errors->has('building_no'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('building_no')}}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="building name">Building Name</label>
                                        <input type="text" class="form-control" name="building_name" value="{{Sentinel::getUser()->building_name}}">
                                    </div>
                                    @if ($errors->has('building_name'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('building_name')}}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="city">City</label>
                                        <input type="text" class="form-control" name="city" value="{{Sentinel::getUser()->city}}">
                                    </div>
                                    @if ($errors->has('city'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('city')}}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="post code">Post Code</label>
                                        <input type="text" class="form-control" name="post_code" value="{{Sentinel::getUser()->post_code}}">
                                    </div>
                                    @if ($errors->has('post_code'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('post_code')}}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="address line1">Address Line 1</label>
                                        <input type="text" class="form-control" name="address_line_one" value="{{Sentinel::getUser()->address_line1}}">
                                    </div>
                                    @if ($errors->has('address_line_one'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('address_line_one')}}</p>
                                    @endif
                                    <div class="form-group">
                                        <label for="address line1">Address Line 2</label>
                                        <input type="text" class="form-control" name="address_line_two" value="{{Sentinel::getUser()->address_line2}}">
                                    </div>
                                    @if ($errors->has('address_line_two'))
                                        <p style="color:#e20b0b">{{'*' . $errors->first('address_line_two')}}</p>
                                    @endif
                                    @if(Sentinel::getUser()->type === 'Medico_Legal_Expert')
                                    <h3>Info </h3>
                                    <hr>
                                    <div class="form-group">
                                        <label for="gmc">GMC</label>
                                        <input type="text" class="form-control" name="gmc" value="{{Sentinel::getUser()->gmc}}">
                                    </div>
                                        @if ($errors->has('gmc'))
                                            <p style="color:#e20b0b">{{'*' . $errors->first('gmc')}}</p>
                                        @endif
                                    <div class="form-group">
                                        <label for="dna">DNA</label>
                                        <input type="text" class="form-control" name="dna" value="{{Sentinel::getUser()->dna}}">
                                    </div>
                                        @if ($errors->has('dna'))
                                            <p style="color:#e20b0b">{{'*' . $errors->first('dna')}}</p>
                                        @endif
                                    <div class="form-group">
                                        <label for="speciality">Speciality</label>
                                        <input type="text" class="form-control" name="speciality" value="{{Sentinel::getUser()->speciality}}">
                                    </div>
                                        @if ($errors->has('speciality'))
                                            <p style="color:#e20b0b">{{'*' . $errors->first('speciality')}}</p>
                                        @endif
                                    <div class="form-group">
                                        <label for="qualification">Qualification</label>
                                        <input type="text" class="form-control" name="qualification" value="{{Sentinel::getUser()->qualification}}">
                                    </div>
                                        @if ($errors->has('qualification'))
                                            <p style="color:#e20b0b">{{'*' . $errors->first('qualification')}}</p>
                                        @endif
                                    <div class="form-group">
                                        <label for="max slot">Max -Slots Per Day</label>
                                        <input type="text" class="form-control" name="max_slot" value="{{Sentinel::getUser()->max_slot}}">
                                    </div>
                                        @if ($errors->has('max_slot'))
                                            <p style="color:#e20b0b">{{'*' . $errors->first('max_slot')}}</p>
                                        @endif
                                    <div class="form-group">
                                        <label for="rate">Appointment Rate</label>
                                        <input type="text" name="appointment_price" class="form-control" value="{{Sentinel::getUser()->appointment_price}}">
                                    </div>
                                        @if ($errors->has('appointment_rate'))
                                            <p style="color:#e20b0b">{{'*' . $errors->first('appointment_rate')}}</p>
                                        @endif
                                    <div class="form-group">
                                        <label for="cv">CV</label>
                                        <input type="file" name="cv" class="form-control"/>
                                    </div>
                                        @if ($errors->has('cv'))
                                            <p style="color:#e20b0b">{{'*' . $errors->first('cv')}}</p>
                                        @endif
                                    <div class="form-group">
                                        <label for="return rate">Returning Fees</label>
                                        <input type="text" name="returning_fees" class="form-control" value="{{Sentinel::getUser()->returning_fees}}">
                                    </div>
                                        @if ($errors->has('returning_fees'))
                                            <p style="color:#e20b0b">{{'*' . $errors->first('returning_fees')}}</p>
                                        @endif
                                    @endif
                                    <button type="submit" class="btn btn-info btn-block"><i class="fa fa-sign-in"></i>&nbsp;Save</button>
                                </form>
                            </div>
                        </div></div>
                </div>
            </div>
        </div>
    </div>
@endsection