@extends('front.inc.master')
@section('title','Dashboard')
@php
    $active = 'dashboard';
    $sideNav = 'dashboard'
@endphp
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-2">
                @include('user.inc.sidenav')
            </div>
            <div class="col-md-10">
                <h2 class="bg-primary" style="padding: 20px;margin-bottom: 10px;box-shadow: 2px 2px 4px 2px rgba(133,130,126,0.31)">Appointment</h2>
                <div class="row">
                    <form  action="/postappointment" method="post" style="margin: 20px">
                        {{csrf_field()}}
                        <input type="hidden" name="doctor_id" value="{{$expert_id}}">
                        <input type="hidden" name="doctor_name" value="{{$expert_name}}">
                        <input type="hidden" name="agency_id" value="{{$agency_id}}">
                        <input type="hidden" name="agency_name" value="{{$agency_name}}">
                        <div class="form-group">
                            <label for="location">Available Location</label>
                            <select class="form-control" name="location" id="exampleFormControlSelect1">
                                <option value="{{$location_one}}">{{$location_one}}</option>
                                <option value="{{$location_two}}">{{$location_two}}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Client Name">Name</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                <span class="glyphicon glyphicon-user"></span>
                                </div>
                                <input type="text" name="client_name" placeholder="Name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Time">Appointment Date</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                                </div>
                                <input type="date" name="date" placeholder="Time" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Time">Appointment Time</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                                <input type="time" name="time" placeholder="Time" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="date_of_birth">DOB</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                                <input type="date" name="dob" placeholder="Date Of birth" class="form-control">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-block btn-large btn-info"><i class="fa fa-plus"></i>&nbsp;Book Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection