@extends('front.inc.master')
@section('title','Reset Password')
@php
    $active = 'Reset Password'
@endphp
@section('body')

    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="text-primary">Reset Password</h2>
                <form action="/user/{{$id}}/reset-password/{{$code}}" method="post" class="form-horizontal" style="margin-top: 50px;margin-bottom: 150px">
                    {{csrf_field()}}
                    {{method_field('PUT')}}
                    <div class="form-group"><label for="new-passwword">New Password:</label><div class="input-group"><div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div><input type="password" class="form-control" id="new-passwword" name="password" placeholder="Password">
                            @if ($errors->has('password'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label for="confirm-password">Confirm New Passowrd:</label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-equalizer"></span></div><input type="password" name="password_confirmation" placeholder="password" class="form-control">
                            @if ($errors->has('password_confirmation'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('password_confirmation')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning btn-block">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection