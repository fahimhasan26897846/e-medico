<ul id="dashboard-navigation">
    <li><a href="/userdashboard" class="@if($sideNav === 'dashboard') {{'side-active-nav'}} @endif"><i class="fa fa-dashboard"></i> &nbsp; Dashboard</a></li>
    <li><a href="/usersettings" class="@if($sideNav === 'settings') {{'side-active-nav'}} @endif"><i class="fa fa-gear"></i> &nbsp;  Account Settings</a></li>
    @if(Sentinel::getUser()->type === 'Medico_Legal_Agency')<li><a href="/searchslot" class="@if($sideNav === 'search slots') {{'side-active-nav'}} @endif"><i class="fa fa-search"></i> &nbsp;  Search Slots</a></li>
    <li><a href="/manage-expert/{{Sentinel::getUser()->id}}" class="@if($sideNav === 'manage experts') {{'side-active-nav'}} @endif"><i class="fa fa-user"></i> &nbsp; Manage Experts</a></li>
@endif
    <li><form action="/logout" id="user-logout" method="post">{{csrf_field()}}<a href="#" onclick="document.getElementById('user-logout').submit()"><i class="fa fa-power-off"></i> Logout</a></form></li>
</ul>