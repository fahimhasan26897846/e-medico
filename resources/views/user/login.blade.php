@extends('front.inc.master')
@section('title','Login')
@php
    $active = 'login'
@endphp
@section('body')

    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="text-primary">Login</h2>
            <form action="/postlogin" method="post" class="form-horizontal" style="margin-top: 50px;margin-bottom: 150px">
                {{csrf_field()}}
                <div class="form-group"><label for="login-email">Email:</label><div class="input-group"><div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div><input type="email" class="form-control" id="login-email" name="email" placeholder="Email">
                        @if ($errors->has('email'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group"><label for="password">Passowrd:</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-equalizer"></span></div><input type="password" name="password" placeholder="password" class="form-control">
                        @if ($errors->has('password'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group"><label for="Remember_me">Remember_me: </label>&nbsp;<input type="checkbox" name="remember_me" ></div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success btn-block">Login</button></div>
                <a href="#" class="pull-left text-primary" data-toggle="modal" data-target="#no-modal">Forget Passord?</a>
                <a href="/show-register" class="pull-right text-primary">Don't have any account?</a>
            </form>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="no-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">ENTER YOUR EMAIL HERE</h5>
                    </div>
                    <div class="modal-body" style="padding: 50px">
                        <form action="/forgetpassword" method="post">
                            {{csrf_field()}}
                        <input type="email" name="email" placeholder="ENTER YOUR EMAIL HERE" class="form-control" style="margin-bottom: 20px">
                        <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection