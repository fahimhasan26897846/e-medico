@extends('front.inc.master')
@section('title','Dashboard')
@php
    $active = 'dashboard';
    $sideNav = 'dashboard'
@endphp
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-2">
                @include('user.inc.sidenav')
            </div>
            <div class="col-md-10">
                <h2 class="bg-primary" style="padding: 20px;border-radius: 5px;margin-bottom: 10px;box-shadow: -1px 5px 20px 0 rgba(126,123,123,0.6)">Dashboard</h2>
                <div class="row">
                    <div class="col-md-6">
                        @if(Sentinel::getUser()->roles()->first()->slug === 'Medico_Legal_Agency')
                        <div style="border-radius: 10px;border: 2px solid rgba(82,82,63,0.32);margin-bottom: 10px"><h3 class="bg-info" style="padding: 10px">What's new today: {{date('d-m-y')}}</h3>
                            <div style="margin: 10px;border: 1px solid #7E7B7B;border-radius: 10px">
                            <div style="border-bottom: 1px solid #000000; padding: 5px">Cases Booked Today <a href="/agency-todays-appointment/{{Sentinel::getUser()->id}}" class="badge badge-success pull-right">{{$thisDay}}</a></div>
                            <div style="border-bottom: 1px solid #000000; padding: 5px">Cases Booked Last 7 Days <a href="/agency-weeks-appointment/{{Sentinel::getUser()->id}}" class="badge badge-success pull-right">{{$thisWeek}}</a></div>
                            <div style="padding: 5px">Cases Booked This Month <a href="/agency-months-appointment/{{Sentinel::getUser()->id}}" class="badge badge-success pull-right">{{$thisMonth}}</a></div>
                            </div>
                        </div>
                            @endif
                            @if(Sentinel::getUser()->roles()->first()->slug === 'Medico_Legal_Expert')
                                <div style="border-radius: 10px;border: 2px solid rgba(82,82,63,0.32);margin-bottom: 10px"><h3 class="bg-info" style="padding: 10px">What's new today: {{date('d-m-y')}}</h3>
                                    <div style="margin: 10px;border: 1px solid #7E7B7B;border-radius: 10px">
                                        <div style="border-bottom: 1px solid #000000; padding: 5px">Cases Booked Today <a href="/experts-todays-appointment/{{Sentinel::getUser()->id}}" class="badge badge-success pull-right">{{$thatDay}}</a></div>
                                        <div style="border-bottom: 1px solid #000000; padding: 5px">Cases Booked Last 7 Days <a href="/experts-weeks-appointment/{{Sentinel::getUser()->id}}" class="badge badge-success pull-right">{{$thatWeek}}</a></div>
                                        <div style="padding: 5px">Cases Booked This Month <a href="/experts-months-appointment/{{Sentinel::getUser()->id}}" class="badge badge-success pull-right">{{$thatMonth}}</a></div>
                                    </div>
                                </div>
                            @endif
                    </div>
                    <div class="col-md-6"><div style="border-radius: 10px;border: 2px solid rgba(82,82,63,0.32)"><h3 class="bg-info" style="padding: 10px">@if(Sentinel::getUser()->type === 'Medico_Legal_Agency'){{'Experts'}}@elseif(Sentinel::getUser()->type === 'Medico_Legal_Expert'){{'Agency'}}@endif  In Your Contract</h3>
                            <div style="margin: 10px;border: 1px solid #7E7B7B;border-radius: 10px">
                                @if(Sentinel::getUser()->type === 'Medico_Legal_Agency')
                                    <div style="padding: 5px">You have <a href="/manage-expert/{{Sentinel::getUser()->id}}" class="badge badge-success">{{$agency}}</a> Expert In your List</div>
                                @elseif(Sentinel::getUser()->type === 'Medico_Legal_Expert')
                                    <div style="padding: 5px">You have <a href="/agency-list/{{Sentinel::getUser()->id}}" class="badge badge-success">{{$doctor}}</a> Agency In your List</div>
                                @endif
                            </div>
                        </div></div>
                </div>
            </div>
        </div>
    </div>
@endsection