@extends('front.inc.master')
@section('title','Register')
@php
    $active = 'Register'
@endphp
@section('body')

    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="text-primary">REGISTER NEW ACCOUNT</h2>
                <form action="/postregister" method="post" class="form-horizontal" style="margin-top: 50px;margin-bottom: 150px">
                    {{csrf_field()}}
                    <div class="form-group"><label for="selectbox">Account Type</label>
                    <select class="form-control" id="selectbox" name="type">
                        <option value="Medico_Legal_Expert">Medico Legal Expert</option>
                        <option value="Medico_Legal_Agency">Medico Legal Agency</option>
                        <option value="Solicitor">Solicitor</option>
                    </select>
                    </div>

                    <div class="form-group"><label for="expert-name" id="DisplayNameL">Expert Full Name:</label><div class="input-group"><div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div><input type="text" class="form-control" id="expert-name" name="name" placeholder="Name"></div></div>

                    <div class="form-group" id="GMCDiv">
                        <label for="gmc">GMC NUMBER:</label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                            <input type="number" name="gmc" id="gmc" class="form-control" placeholder="GMC NUMBER">
                            @if ($errors->has('gmc'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('gmc')}}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group"><label for="contact">Contact:</label>
                        <div class="input-group"><div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                            <input type="number" class="form-control" id="contact" name="contact" placeholder="Contact">
                            @if ($errors->has('contact'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('contact')}}</p>
                            @endif
                        </div>
                    </div>

                    <div class="form-group"><label for="login-email">Email:</label><div class="input-group"><div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div><input type="email" class="form-control" id="login-email" name="email" placeholder="Email">
                            @if ($errors->has('email'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="form-group"><label for="password">Passowrd:</label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-equalizer"></span></div><input type="password" name="password" placeholder="password" class="form-control">
                            @if ($errors->has('password'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                            @endif
                        </div></div>

                    <div class="form-group"><label for="password">Confirm Passowrd:</label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-equalizer"></span></div><input type="password" name="confirm_password" placeholder="password" class="form-control">
                            @if ($errors->has('confirm_password'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('confirm_password')}}</p>
                            @endif
                        </div></div>
                    <div class="form-group"><input type="checkbox" name="agree" >&nbsp;<label for="Remember_me">I aggree the
                            <a href="#" data-toggle="modal" data-target="#exampleModalLong">Terms and conditions</a> of E-medico </label>&nbsp;
                        @if ($errors->has('agree'))
                            <p style="color:#e20b0b">{{'*' . $errors->first('agree')}}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success btn-block">Register</button></div>
                    <a href="/show-login" class="pull-right text-primary">Already have an account?</a>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Our Terms And Conditions</h3>
                </div>
                <div class="modal-body" style="padding: 60px">
1. Acceptance of Terms of Use:
By accessing and using this Site you agree to be bound by the following Terms of Use and all terms and conditions contained and/or referenced herein or any additional terms and conditions set forth on this Site and all such terms shall be deemed accepted by you. If you do NOT agree to all these Terms of Use, you should NOT use this Site. If you do not agree to any additional specific terms which apply to particular Content (as defined below) or to particular transactions concluded through this Site, then you should NOT use the part of the Site which contains such Content or through which such transactions may be concluded and you should not use such Content or conclude such transactions.

These Terms of Use may be amended at any time upon which prior notification will be provided. Such amended Terms of Use shall be effective upon posting on this Site. Please check the Terms of Use published on this Site regularly to ensure that you are aware of all terms governing your use of this Site. Other circohealthcare Web sites may have their own terms of use or privacy statement, which apply to such Web sites. Also, specific terms and conditions may apply to specific content, products, materials, services, discussion forums, or information contained on or available through this Site (the "Content") or transactions concluded through this Site. Such specific terms may supplement these Terms of Use or, where inconsistent with these Terms of Use, such specific terms will supersede these Terms of Use only to the extent that the content or intent of such specific terms is inconsistent with these Terms of Use.

circohealthcare reserves the right to make changes or updates with respect to or in the Content of the Site or the format thereof at any time without notice. circohealthcare reserves the right to terminate or restrict access to the Site for any reason whatsoever at its sole discretion.

2. Laws and Regulations:
Your access to and use of this Site is subject to all applicable international, federal, state and local laws and regulations. You agree not to use the Site in any way that violates such laws or regulations.

3. Copyright Notice:
Copyright and all other proprietary rights in the Content (including but not limited to software, audio, video, text and photographs) rest with circohealthcare or its registered members. All rights in the Content not expressly granted herein are reserved. Except as stated herein, none of the material may be copied, reproduced, modified, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means, including, but not limited to, electronic, mechanical, photocopying, recording, or otherwise, without the prior written permission of circohealthcare or the copyright owner. All copyright and other proprietary notices shall be retained on all permitted reproductions. Permission is granted to display, copy, distribute and download the materials on this Site for personal, non-commercial use only, provided you do not modify the materials and that you retain all copyright and other proprietary notices contained in the materials. You also may not, without circohealthcare's permission, "mirror" any material contained on this Site on any other server. This permission terminates automatically if you breach any of these terms or conditions. Upon termination, you must immediately destroy any downloaded and/or printed materials. Any unauthorized use of any material contained on this Site may violate copyright laws, trademark laws, the laws of privacy, communications, regulations and statutes.

Any material or information sent through or in connection with this Site by you ("User Materials") will be treated as non-confidential and non-proprietary, and immediately become the property of circohealthcare, subject to the applicable Privacy Statement posted on this Site. circohealthcare may use such User Materials as it deems fit, anywhere in the world, without obligation for compensation, and free of any moral rights, intellectual property rights and/or other proprietary rights in or to such User Materials.

This Site may contain references to specific circohealthcare products and services that may not be (readily) available in a particular country. Any such reference does not imply or warrant that any such products or services shall be available at any time in any particular country. Please contact your local circohealthcare business contact for further information.

4. Trademarks:
Unless otherwise indicated, all trademarks, service marks and logos displayed on the Site are registered and unregistered trademarks of circohealthcare (or its affiliates, subsidiaries or divisions). These include each of circohealthcare’s primary product brands and service offerings and its corporate logos and emblems.

5. Tampering:
User agrees not to modify, move, add to, delete or otherwise tamper with the information contained in circohealthcare's Site. User also agrees not to decompile, reverse engineer, disassemble or unlawfully use or reproduce any of the software, copyrighted or trademarked material, trade secrets, or other proprietary information contained in the Site.

6. Indemnity:
You agree to defend, indemnify, and hold harmless circohealthcare, its affiliates, subsidiaries, and divisions, and each of their officers, directors, employees and agents, from and against any claims, actions or demands, including without limitation reasonable legal and accounting fees, alleging or resulting from your use of the Content (including software) or your breach of the terms of this Agreement. circohealthcare shall provide notice to you promptly of any such claim, suit, or proceeding and shall assist you, at your expense, in defending any such claim, suit or proceeding.

7. Third Party Information:
Although circohealthcare monitors the information on the Site, some of the information may be supplied by independent third parties. While circohealthcare makes every effort to insure the accuracy of all information on the Site, circohealthcare makes no warranty as to the accuracy of any such information.

8. Links To Third Party Sites:
This Site may contain links that will let you access other Web sites that are not under the control of circohealthcare. The links are only provided as a convenience and circohealthcare does not endorse any of these sites. circohealthcare assumes no responsibility or liability for any material that may accessed on other Web sites reached through this Site, nor does circohealthcare make any representation regarding the quality of any product or service contained at any such site.

9. Links From Third Party Sites:
circohealthcare prohibits unauthorized links to the Site and the framing of any information contained on the site or any portion of the Site. circohealthcare reserves the right to disable any unauthorized links or frames. circohealthcare has no responsibility or liability for any material on other Web sites that may contain links to this Site.

10. No Warranties:
Although care has been taken to ensure the accuracy of information on this Site, circohealthcare assumes no liability therefore. Information and documents provided on this Site are provided "AS IS" and “AS AVAILABLE”. circohealthcare hereby disclaims any representations and warranties of any kind, either express or implied, including without limitation warranties of merchantability, fitness for a particular purpose, non-infringement, title or as to operation or content. circohealthcare uses reasonable efforts to include accurate and up-to-date information on this Site; it does not, however, make any warranties or representations as to its accuracy or completeness. circohealthcare periodically adds, changes, improves, or updates the information and documents on this Site (including but not limited to products, pricing, programs, events, and offers) without notice. circohealthcare assumes no liability or responsibility for any errors or omissions in the content of this Site. Your use of this Site is at your own risk.

11. Limitation Of Liability:
Under no circumstances shall circohealthcare or any of its affiliates, subsidiaries, or divisions, be liable for any damages suffered by you, including any incidental, special or consequential damages (including, without limitation, any lost profits or damages for business interruption, loss of information, programs or other data) that result from access to, use of, or inability to use this site or due to any breach of security associated with the transmission of information through the internet, even if circohealthcare was advised of the possibility of such damages. Any action brought against circohealthcare pertaining to or in connection with this site must be commenced and notified to circohealthcare in writing within one (1) year after the date the cause for action arose.

12. Privacy:
Protecting the privacy of our clients and users of our Sites is important to circohealthcare. The circohealthcare Privacy Statement, as updated and amended from time to time, describes how we use and protect information you provide to us.

13. Security:
The security of information transmitted through the Internet can never be guaranteed. circohealthcare does not warrant or make any representations as to the security of this site. You acknowledge any information sent may be intercepted. circohealthcare does not warrant that the site or the servers, which make this site available or electronic communications sent by circohealthcare are free from viruses or any other harmful elements. circohealthcare is not responsible for any interception or interruption of any communications through the Internet or for changes to or losses of data. You are responsible for maintaining the security of any password, user ID, or other form of authentication involved in obtaining access to password protected or secure areas of circohealthcare sites. In order to protect you and your data, circohealthcare may suspend your use of a client site, without notice, pending an investigation, if any breach of security is suspected.

14. Transmission Of Personal Data:
You acknowledges and agree that by providing circohealthcare with any personal information through the Site, you consent to the transmission of such personal information over international borders as necessary for processing in accordance with circohealthcare's standard business practices and the applicable Privacy Statement.

15. Access To Password Protected/Secure Areas:
Access to and use of password protected and/or secure area of the Site is restricted to authorized users only. Unauthorized access to such areas is prohibited and may lead to criminal prosecution.

16. Forward-Looking Statements:
The information on this Site may contain certain projections or forward-looking statements with respect to the financial condition, results of operations and business of circohealthcare or any of its affiliates and certain plans and objectives of circohealthcare with respect to these items. By their nature, projections and forward-looking statements involve risk and uncertainty because they relate to events and depend on circumstances that will occur in the future. There are a number of factors that could cause actual results and developments to differ materially from those expressed or implied by these projections or forward-looking statements. These factors include, but are not limited to, levels of business spending in major economies, the levels of marketing and promotional expenditures by circohealthcare and its competitors, raw materials and employee costs, changes in future exchange and interest rates (in particular, changes in the euro and the US dollar can materially affect results), changes in tax rates and future business combinations, acquisitions or dispositions and the rate of technical changes.

17. Specific Software Available On This Site:
Any software that may be made available to download from the Site (the "Software") is the copyrighted work of circohealthcare and/or a third party providing that Software. Software made available for downloading from or through this Site is licensed subject to the terms of the applicable license agreement, which may accompany or be included with the Software ("License Agreement"). An end user may be unable to install any Software that is accompanied by or includes a License Agreement, unless the end user first agrees to the License Agreement terms. Except as set forth in the applicable License Agreement, the Software is made available for use by end users only and any further copying, reproduction or redistribution of the Software is expressly prohibited. Warranties, if any, with respect to such software shall only apply as expressly set forth in the applicable license agreement. circohealthcare hereby expressly disclaims all further representations and warranties of any kind, express or implied, including warranties of merchantability, fitness for any particular purpose, title or non-infringement with respect to the software.

18. Jurisdiction/Governing Law:
These Terms of Use shall be governed by and construed in accordance with the laws of The Netherlands without regard to its principles of conflict of laws. You agree that any disputes in connection with this agreement or its enforcement shall be resolved in a court of competent jurisdiction in The Netherlands. In the event that any provision of the Terms of Use are deemed by a court of competent jurisdiction to be invalid or unenforceable, the invalid portion of the Terms of Use shall be considered to be modified as closely as possible to the intent of circohealthcare and the remainder of the Terms of Use shall remain in full force and effect.

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
    $('#selectbox').change(function () {
    SetFields();
    });

    function SetFields() {
    if ($('#selectbox').val() === 'Solicitor') {
    $('#gmc').val('-100');
    $('#GMCDiv').hide();
    $('#DisplayNameL').html('Solicitor Full Name');
    }
    else if ($('#selectbox').val() === 'Medico_Legal_Agency') {
    $('#gmc').val('-100');
    $('#GMCDiv').hide();
    $('#DisplayNameL').html('Agency Full Name');
    }
    else if ($('#selectbox').val() === 'Medico_Legal_Expert') {
    $('#gmc').val('');
    $('#GMCDiv').show();
    $('#DisplayNameL').html('Expert Full Name');
    }
    }
    </script>

@endsection