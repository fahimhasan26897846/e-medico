@extends('front.inc.master')
@section('title','Dashboard')
@php
    $active = 'dashboard';
    $sideNav = 'manage experts'
@endphp
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <div class="container" style="margin-top: 50px;margin-bottom: 50px">
        <div class="row">
            <div class="col-md-2">
                @include('user.inc.sidenav')
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-6">
                        <h4 class="btn-info text-center" style="padding: 10px;margin-bottom: 10px">Experts In your Lists</h4>
                        @foreach($users as $flight)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h5 class="panel-title asd">
                                        <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{$flight->id}}" aria-expanded="false"
                                           aria-controls="collapseOne">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>{{$flight->name}}
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseOne{{$flight->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"
                                     style="height: 0px;">
                                    <div class="panel-body panel_text">
                                        GMC: {{$flight->gmc}} <br>
                                        Qualification: {{$flight->qualification}} <br>
                                        Max Slot: {{$flight->max_slot}} <br>
                                        Appointment Fees: {{$flight->appointment_price}} <br>
                                        Returning Fees: {{$flight->returning_fees}} <br>
                                        DNA: {{$flight->dna}} <br>
                                        <div class="row d-flex">@if($flight->cv != NULL)<a class="btn-sm btn btn-success" href="CV/{{$flight->cv}}" download="{{$flight->name}}"><i class="fa fa-file-pdf-o"></i>&nbsp;CV</a>@endif <a class="btn btn-warning btn-sm" href="#" data-toggle="modal" data-target="#exampleModallocation{{$flight->id}}"><i class="fa fa-location-arrow"></i>&nbsp;Location</a> <a class="btn btn-danger btn-sm" href="/experts/delete/{{$flight->id}}"><i class="fa fa-trash"></i>&nbsp;Remove</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="exampleModallocation{{$flight->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Locations</h5>
                                        </div>
                                        <div class="modal-body">
                                            Buliding No: {{$flight->building_no}} <br>
                                            Builder_name: {{$flight->building_name}}<br>
                                            City: {{$flight->city}}<br>
                                            Post Code: {{$flight->post_code}}<br>
                                            Location 1:<br>{{$flight->address_line1}}<br>
                                            Location 2: <br> {{$flight->address_line2}}<br>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-6">
                        <h4 class="btn-primary text-center" style="padding: 10px;margin-bottom: 10px">Experts Availables</h4>
                        @foreach($others as $other)
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h5 class="panel-title asd">
                                        <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsedOne{{$other->id}}" aria-expanded="false"
                                           aria-controls="collapseOne">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>{{$other->name}}
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapsedOne{{$other->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"
                                     style="height: 0px;">
                                    <div class="panel-body panel_text">
                                        GMC: {{$other->gmc}} <br>
                                        Qualification: {{$other->qualification}} <br>
                                        Max Slot: {{$other->max_slot}} <br>
                                        Appointment Fees: {{$other->appointment_price}} <br>
                                        Returning Fees: {{$other->returning_fees}} <br>
                                        DNA: {{$other->dna}} <br>
                                        <div class="row d-flex">@if($other->cv != NULL)<a class="btn-sm btn btn-success" href="CV/{{$other->cv}}" download><i class="fa fa-file-pdf-o"></i>&nbsp;CV</a>@endif <a class="btn btn-warning btn-sm" href="#" data-toggle="modal" data-target="#examplelocation{{$other->id}}"><i class="fa fa-location-arrow"></i>&nbsp;Location</a>
                                            <form action="/addexpert" method="post">
                                                {{csrf_field()}}
                                                <input type="hidden" name="agency_id" value="{{Sentinel::getUser()->id}}">
                                                <input name="experts_id" value="{{$other->id}}" type="hidden">
                                                <input type="hidden" value="{{$other->email}}">
                                                <input type="hidden" value="{{Sentinel::getUser()->name}}">
                                                <input type="hidden" value="{{Sentinel::getUser()->contact}}">
                                                <button  type="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Add</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="examplelocation{{$other->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Locations</h5>
                                        </div>
                                        <div class="modal-body">
                                            Buliding No: {{$other->building_no}} <br>
                                            Builder_name: {{$other->building_name}}<br>
                                            City: {{$other->city}}<br>
                                            Post Code: {{$other->post_code}}<br>
                                            Location 1:<br>{{$other->address_line1}}<br>
                                            Location 2: <br> {{$other->address_line2}}
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection