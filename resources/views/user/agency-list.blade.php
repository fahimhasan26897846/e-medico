@extends('front.inc.master')
@section('title','Dashboard')
@php
    $active = 'dashboard';
    $sideNav = 'dashboard'
@endphp
@section('style')
    <style href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"></style>
@endsection
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-2">
                @include('user.inc.sidenav')
            </div>
            <div class="col-md-10">
                <h2 class="bg-primary" style="padding: 20px;margin-bottom: 10px;box-shadow: 2px 2px 4px 2px rgba(133,130,126,0.31)">Agency</h2>
                <div class="row">
                    <table id="example" class="display bg-default table table-hover" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Agency Name</th>
                            <th>Appointment Email</th>
                            <th>Agency Contact</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Agency Name</th>
                            <th>Appointment Email</th>
                            <th>Agency Contact</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($agencies as $agency)
                            <tr>
                                <td>{{$agency->name}}</td>
                                <td>{{$agency->email}}</td>
                                <td>{{$agency->contact}}</td>
                                <td><a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp; Remove Agency</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection