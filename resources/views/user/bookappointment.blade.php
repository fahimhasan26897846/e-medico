@extends('front.inc.master')
@section('title','Dashboard')
@php
    $active = 'dashboard';
    $sideNav = 'search slots'
@endphp
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <div class="container" style="margin-top: 50px;margin-bottom: 50px">
        <div class="row">
            <div class="col-md-2">
                @include('user.inc.sidenav')
            </div>
            <div class="col-md-10">
                <div class="row">
                    <input type="text" name="search" class="form-control" id="search" placeholder="Search with Post Code" style="margin-bottom:10px "></div>
                <div class="row">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">Serial</th>
                            <th scope="col">Name</th>
                            <th scope="col">Post Code</th>
                            <th scope="col">Slots Avilable</th>
                            <th scope="col">Book Appointment</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $('#search').on('keyup',function(){
            $value  = $(this).val();
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/search-expert')}}',
                data : {'search':$value},
                success :function (data) {
                    $('tbody').html(data);
                }
            })
        })
    </script>
@endsection