@extends('front.inc.master')
@section('title','Contact')
@php
$active = 'contact'
@endphp
@section('body')

<!-- banner -->
<div class="banner_inner_content_agile_w3l">

</div>
<!--//banner -->
<!-- /inner_content -->
<div class="banner-bottom">
    <div class="container">
        <div class="inner_sec_info_agileits_w3">
            <h2 class="heading-agileinfo">Mail Us<span>We offer extensive medical procedures to outbound and inbound patients.</span></h2>
            <div class="contact-form">
                <form method="post" action="/post-message">
                    {{csrf_field()}}
                    <div class="left_form">
                        <div>
                            <span><label>Name</label></span>
                            <span><input name="name" type="text" class="textbox" required=""></span>
                            @if ($errors->has('name'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('name')}}</p>
                            @endif
                        </div>
                        <div>
                            <span><label>E-mail</label></span>
                            <span><input name="email" type="text" class="textbox" required=""></span>
                            @if ($errors->has('email'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                            @endif
                        </div>
                        <div>
                            <span><label>Contact</label></span>
                            <span><input name="phone" type="text" class="textbox" required=""></span>
                            @if ($errors->has('phone'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('phone')}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="right_form">
                        <div>
                            <span><label>Message</label></span>
                            <span><textarea name="message" required=""> </textarea></span>
                            @if ($errors->has('message'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('message')}}</p>
                            @endif
                        </div>
                        <div>
                            <span><input type="submit" value="Submit" class="myButton"></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>


    </div>
</div>
@endsection