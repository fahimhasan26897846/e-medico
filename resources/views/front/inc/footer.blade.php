<!-- footer -->
<div class="footer_top_agile_w3ls">
    <div class="container">
        <div class="col-md-3 footer_grid">
            <h3>About Us</h3>
            <p>Circo Health Care is online appointment booking website. Come Here to book your appointment.

            </p>

        </div>
        <div class="col-md-3 footer_grid">
            <h3>Contents</h3>
            <ul class="footer_grid_list">
                <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a href="/" >Home </a>
                </li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a href="/about" >About Us</a>
                </li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a href="/faq" >Faq </a>
                </li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a href="/contact" >Contact</a>
                </li>
                <li><i class="fa fa-angle-right" aria-hidden="true"></i>
                    <a href="/pricing" >Pricing</a>
                </li>
            </ul>
        </div>
        <div class="col-md-3 footer_grid">
            <h3>Contact Info</h3>
            <ul class="address">
                <li><i class="fa fa-map-marker" aria-hidden="true"></i>8088 USA, Honey block, <span>New York City.</span></li>
                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="{{asset('front-page-resource/')}}mailto:info@example.com">info@example.com</a></li>
                <li><i class="fa fa-phone" aria-hidden="true"></i>+09187 8088 9436</li>
            </ul>
        </div>
        <div class="col-md-3 footer_grid ">
            <h3>Sign up for our Newsletter</h3>
            <p>Get Started For Free</p>
            <div class="footer_grid_right">

                <form action="/post-email-newsletter" method="post">
                    {{csrf_field()}}
                    <input type="email" name="email" placeholder="Email Address..." required="">
                    <input type="submit" value="Submit">
                </form>
            </div>
        </div>
        <div class="clearfix"> </div>

    </div>
</div>
<div class="footer_wthree_agile">
    <p>Copyright © {{ date('Y') }} Circohealthcare.co.uk. All rights reserved </p>
</div>
<!-- //footer -->