@php session_start(); @endphp
<!DOCTYPE html>
<html>

<head>
    <title>@yield('title')</title>
    <!--/tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--//tags -->
    @yield('style')
    <link href="{{asset('front-page-resource/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('front-page-resource/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('front-page-resource/css/appointment_style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('front-page-resource/css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('admin/plugins/bower_components/sweetalert/sweetalert.css')}}">
    <!-- //for bootstrap working -->
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700" rel="stylesheet">
    <style>
        .form-control{background-color: #ffffff !important; border: 1px solid #c0c0c0 !important;}
    </style>
</head>

<body>
@include('front.inc.header')

@yield('body')
@include('front.inc.footer')
<!-- js -->
<script type="text/javascript" src="{{asset('front-page-resource/js/jquery-2.1.4.min.js')}}"></script>
<script>
    $('ul.dropdown-menu li').hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
</script>
<script type="text/javascript" src="{{asset('front-page-resource/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/plugins/bower_components/sweetalert/sweetalert.min.js')}}"></script>

@yield('script')
<script>
    @if(session('success'))
        swal("{{session('success')}}");
    @endif
    @if(session('error'))
    swal("{{session('error')}}");
    @endif
</script>
</body>
</html>