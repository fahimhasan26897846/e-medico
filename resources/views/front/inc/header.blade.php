<!-- header -->
<div class="header" id="home">
    <div class="top_menu_w3layouts">
        <div class="container">
            <div class="header_left">
                <ul>
                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> 1143 New York, USA</li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i> +(010) 221 918 811</li>
                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com">info@example.com</a></li>
                </ul>
            </div>
            <div class="header_right">
                <ul class="forms_right">
                    @if(Sentinel::check())
                        <li style="color: #ffffff;padding-right: 10px"> Hello {{Sentinel::getUser()->name}} </li>
                    @if(Sentinel::getUser()->roles()->first()->slug != 'Admin')
                        <li><a href="/userdashboard">Dashboard</a></li>
                        <li><form action="/logout" id="user-logout" method="post">{{csrf_field()}}<a href="#" onclick="document.getElementById('user-logout').submit()"><i class="fa fa-power-off"></i> Logout</a></form></li>
                        @elseif(Sentinel::getUser()->roles()->first()->slug === 'Admin')
                            <li><a href="/dashboard">Dashboard</a></li>
                            <li><form action="/admin-logout" id="user-logout" method="post">{{csrf_field()}}<a href="#" onclick="document.getElementById('user-logout').submit()"><i class="fa fa-power-off"></i> Logout</a></form></li>
                        @endif
                    @else
                    <li><a href="/show-register">Register</a> </li>
                    <li><a href="/show-login">Login</a></li>
                    @endif
                </ul>

            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="content white">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <h1><span class="fa fa-stethoscope" aria-hidden="true"></span>New Clinic </h1>
                    </a>
                </div>
                <!--/.navbar-header-->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav>
                        <ul class="nav navbar-nav">
                            <li><a href="/" class="@if($active === 'home'){{ 'active' }}@endif">Home</a></li>
                            <li><a href="/pricing" class="@if($active === 'pricing'){{ 'active' }}@endif">Pricing</a></li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle @if($active === 'about' || $active === 'faq'){{ 'active' }}@endif" data-toggle="dropdown">Info <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/about">About Us</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/faq">Faq</a></li>
                                    <li class="divider"></li>

                                </ul>
                            </li>
                            <li><a href="/contact" class="@if($active === 'contact'){{ 'active' }}@endif">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
                <!--/.navbar-collapse-->
                <!--/.navbar-->
            </div>
        </nav>
    </div>
</div>