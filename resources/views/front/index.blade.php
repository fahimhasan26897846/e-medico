@extends('front.inc.master')
@section('title','E-medico')
@php
    $active = 'home'
@endphp
@section('body')
<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
        <li data-target="#myCarousel" data-slide-to="2" class=""></li>
        <li data-target="#myCarousel" data-slide-to="3" class=""></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="container">
                <div class="carousel-caption">
                    <h3>With a Touch of <span>Kindness.</span></h3>
                    <p>Child Care Treatments</p>
                    <h6>Our Medical Center is the preferred choice for diplomats and employees from 64 embassies, consulates and UN agencies, as well as private patients from over 60 countries.</h6>
                </div>
            </div>
        </div>
        <div class="item item2">
            <div class="container">
                <div class="carousel-caption">
                    <h3>With a Touch of <span>Kindness.</span></h3>
                    <p>Child Care Treatments</p>
                    <h6>Our Medical Center is the preferred choice for diplomats and employees from 64 embassies, consulates and UN agencies, as well as private patients from over 60 countries.</h6>
                </div>
            </div>
        </div>
        <div class="item item3">
            <div class="container">
                <div class="carousel-caption">
                    <h3>With a Touch of <span>Kindness.</span></h3>
                    <p>Child Care Treatments</p>
                    <h6>Our Medical Center is the preferred choice for diplomats and employees from 64 embassies, consulates and UN agencies, as well as private patients from over 60 countries.</h6>
                </div>
            </div>
        </div>
        <div class="item item4">
            <div class="container">
                <div class="carousel-caption">
                    <h3>With a Touch of <span>Kindness.</span></h3>
                    <p>Child Care Treatments</p>
                    <h6>Our Medical Center is the preferred choice for diplomats and employees from 64 embassies, consulates and UN agencies, as well as private patients from over 60 countries.</h6>
                </div>
            </div>
        </div>
    </div>
    <a class="left carousel-control" href="{{asset('front-page-resource/')}}#myCarousel" role="button" data-slide="prev">
        <span class="fa fa-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="{{asset('front-page-resource/')}}#myCarousel" role="button" data-slide="next">
        <span class="fa fa-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!-- The Modal -->
</div>
<!--//banner -->
<!-- about -->
<div class="agile-about w3ls-section">
    <!-- about-bottom -->
    <div class="agileits-about-btm">
        <h3 class="heading-agileinfo">Welcome To Our Clinic!<span>We offer extensive medical procedures to outbound and inbound patients.</span></h3>
        <div class="container">
            <div class="w3-flex">
                <div class="col-md-4 col-sm-4 ab1 agileits-about-grid1">
                    <h4 class="agileinfo-head">For Adults</h4>
                    <p>You can call our highly experienced clinical team if your child, teenager or family is going through difficulties.</p>
                    <h5>ANXIETY </h5>
                    <h5>DEPRESSION</h5>
                    <h5>ADHD </h5>
                    <h5>BIPOLAR DISORDER</h5>
                    <h5>A – Z OF ISSUES</h5>
                </div>
                <div class="col-md-4 col-sm-4 ab1 agileits-about-grid2">

                    <h4 class="agileinfo-head">For Children</h4>
                    <p>You can call our highly experienced clinical team if your child, teenager or family is going through difficulties.</p>
                    <h5>ADHD </h5>
                    <h5>ASPERGERS AND AUTISM</h5>
                    <h5>DEPRESSION</h5>
                    <h5>ANXIETY DISORDERS</h5>
                    <h5>A – Z OF ISSUES</h5>
                </div>
                <div class="col-md-4 col-sm-4 ab1 agileits-about-grid3">

                    <h4 class="agileinfo-head">For Business</h4>
                    <p>You can call our highly experienced clinical team if your child, teenager or family is going through difficulties.</p>
                    <h5>MEDICO LEGAL REPORTS </h5>
                    <h5>OCCUPATIONAL HEALTH</h5>
                    <h5>LOCUMS AND STAFFING</h5>
                    <h5>NEUROPSYCHOLOGICAL ASSESSMENTS</h5>
                    <h5>MENTAL CAPACITY ASSESSMENTS</h5>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //about-bottom -->
</div>
<!-- emergency -->
<div class="emergency_cases_w3ls">
    <div class="emergency_cases_bt">
        <div class="container">
            <div class="emergency_cases_top">
                <div class="col-md-6 emergency_cases_w3ls_left">
                    <h4>Opening Hours</h4>
                    <h6>Monday - Friday&nbsp;<span class="eme">8.00 - 18.00</span></h6>
                    <h6>Monday - Saturday&nbsp;<span class="eme">9.00 - 17.00</span></h6>
                    <h6>Monday - Sunday&nbsp;<span class="eme">9.00 - 15.00</span></h6>
                </div>
                <div class="col-md-6 emergency_cases_w3ls_right">
                    <h4>Emergency Cases</h4>
                    <h5><i class="fa fa-phone" aria-hidden="true"></i>1230456789</h5>
                    <p>Your treatment plan is designed for steady progress, with every phase promptly implemented.</p>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- //emergency -->
<!-- services -->
<div class="services">
    <div class="container">
        <h3 class="heading-agileinfo">Therapies & Treatments<span>We offer extensive medical procedures to outbound and inbound patients.</span></h3>

        <div class="services-top-grids">
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-heartbeat" aria-hidden="true"></i>
                    <h4>Anxiety</h4>
                    <p>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-user-md" aria-hidden="true"></i>
                    <h4>Executive Coaching</h4>
                    <p>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-wheelchair-alt" aria-hidden="true"></i>
                    <h4>Depression</h4>
                    <p>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="services-bottom-grids">
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-medkit" aria-hidden="true"></i>
                    <h4>Relationships</h4>
                    <p>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-hospital-o" aria-hidden="true"></i>
                    <h4>Stress Management</h4>
                    <p>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="grid1">
                    <i class="fa fa-ambulance" aria-hidden="true"></i>
                    <h4>Support Group</h4>
                    <p>Lorem ipsum dolor sit amet, Sed ut perspiciatis unde omnis iste natus error sit voluptatem </p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //services -->
@endsection
