@extends('front.inc.master')
@section('title','Faq')
@php
$active = 'faq'
@endphp
@section('body')
    <!-- about -->
    <div class="about">
        <div class="container">
            <h3 class="heading-agileinfo">What We Do<span>We creates a inner relation between the medical eperts and patients.</span></h3>
            <div class="col-md-6 about-w3right">
                <img src="{{asset('front-page-resource/images/g6.jpg')}}" class="img-responsive" alt="" />
            </div>
            <div class="col-md-6 about-w3left">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    @foreach($faqs as $faq)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h5 class="panel-title asd">
                                <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                                   aria-controls="collapseOne">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>{{$faq->questions}}
                                </a>
                            </h5>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"
                             style="height: 0px;">
                            <div class="panel-body panel_text">
                               {{$faq->answer}}
                            </div>
                        </div>
                    </div>
                        @endforeach
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //about -->
@endsection