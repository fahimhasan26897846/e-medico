@extends('front.inc.master')
@section('title','Pricing')
@php
$active = 'pricing'
@endphp
@section('body')
    <!-- banner -->
    <div class="banner_inner_content_agile_w3l">

    </div>
    <!--//banner -->
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div><h1 style="margin-bottom: 50px">For Medical Experts</h1><h3 style="margin-bottom: 20px">E-medico Pricing for Medico Legal Experts</h3></div>
            <div style="margin-bottom: 50px"><p class="bg-info" style="padding: 5px;margin: 10px;font-family: lato;font-size: 15pt">£1.50 for every successful appointment booked via EasySlot by an instructing party.</p>
                <p class="bg-info" style="padding: 5px;margin: 10px;font-family: lato;font-size: 15pt">There will be no charge for DNA or Cancelled Appointments</p>
                <p class="bg-info" style="padding: 5px;margin: 10px;font-family: lato;font-size: 15pt">Experts having more than 300 Appointments per month can contact us for discounted rates.</p></div>
            <div style="margin-bottom: 50px"><h3 style="margin-bottom: 50px">For Instructing Parties</h3><h5 style="margin-bottom: 20px">
                    E-medico Pricing for Instructing Parties</h5><p class="bg-info" style="padding: 5px;margin: 10px;font-family: lato;font-size: 15pt">FREE to use for Medical Agencies.</p></div>
        </div>
    </div>
@endsection