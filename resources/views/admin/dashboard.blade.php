@extends('admin.inc.master')
@section('title','Dashboard')
@section('body')

        <div class="container-fluid">
            <div class="row bg-title">
                <!-- .page title -->
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">DashBoard</h4>
                </div>
                <!-- /.page title -->
                <!-- .breadcrumb -->
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/dashboard">Dashboard</a></li>
                    </ol>
                </div>
                <!-- /.breadcrumb -->
            </div>
            <!-- .row -->
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="white-box">
                        <div class="r-icon-stats">
                            <i class="ti-user bg-megna"></i>
                            <div class="bodystate">
                                <h4>{{$agency}}</h4>
                                <span class="text-muted">Agency</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="white-box">
                        <div class="r-icon-stats">
                            <i class="ti-shopping-cart bg-info"></i>
                            <div class="bodystate">
                                <h4>{{$experts}}</h4>
                                <span class="text-muted">Experts</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="white-box">
                        <div class="r-icon-stats">
                            <i class="ti-wallet bg-success"></i>
                            <div class="bodystate">
                                <h4>{{$appointments}}</h4>
                                <span class="text-muted">Total Appointment</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="white-box">
                        <div class="r-icon-stats">
                            <i class="ti-wallet bg-inverse"></i>
                            <div class="bodystate">
                                <h4>346505654</h4>
                                <span class="text-muted">Total Visit</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- .row -->
        </div>

@endsection

