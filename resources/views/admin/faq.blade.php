@extends('admin.inc.master')
@section('title','Dashboard')
@section('body')

    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Faqs page</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li class="active">Faqs page</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-outline-primary mb-3 mt-3 btn-lg" href="#" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i>&nbsp; Create FAQ</a>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Create An new faq</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="/admin-postfaq" method="post">
                            <div class="modal-body">
                               {{csrf_field()}}
                                <div class="form-group">
                                    <label for="question">Question</label>
                                    <input type="text" name="question" class="form-control" required="">
                                </div>
                                <div class="form-group">
                                    <label for="answer">Answer</label>
                                    <input type="text" name="answer" class="form-control" required="">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Create</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    @foreach($faqs as $faq)
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="font-bold"> {{$faq->questions}} </a> </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body"> {{$faq->answer}}
                                <a href="#" class="fa fa-pencil pull-right" data-toggle="modal" data-target="#example{{$faq->id}}"></a></div>
                            <div class="modal fade" id="example{{$faq->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Update faq</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form action="/admin-updatefaq" method="post">
                                            <div class="modal-body">
                                                {{csrf_field()}}
                                                <input type="hidden" name="id" value="{{$faq->id}}">
                                                <div class="form-group">
                                                    <label for="question">Question</label>
                                                    <input type="text" name="question" class="form-control" value="{{$faq->questions}}" required="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="answer">Answer</label>
                                                    <input type="text" name="answer" class="form-control" value="{{$faq->answer}}" required="">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-secondary">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection

