@extends('admin.inc.master')
@section('title','Dashboard')
@section('body')

    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">DashBoard</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li>Admin List</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row bg-title">
            <table class="table table-responsive table-hover table-bordered table-dark">
                <thead class="thead-light">
                <tr>
                <th scope="col">Serial</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Contact</th>
                <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $serial = 1 ?>
                @foreach($admins as $user)
                <tr>
                <th scope="row">{{$serial}}</th>
                <th>{{$user->name}}</th>
                <th>{{$user->email}}</th>
                <th>{{$user->contact}}</th>
                <th>@if($user->id != Sentinel::getUser()->id)<a href="/deleteadmin/{{$user->id}}" class="btn btn-sm btn-rounded btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete Admin</a>@endif</th>
                </tr>
                    @php($serial++)
                @endforeach
                </tbody>
            </table>
        </div>
        <!-- .row -->
    </div>

@endsection

