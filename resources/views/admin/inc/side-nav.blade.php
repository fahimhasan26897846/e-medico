<!-- Left navbar-header -->
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                <!-- input-group -->
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                <!-- /input-group -->
            </li>
            <li class="user-pro">
                <a href="#" class="waves-effect"><img src="{{asset('admin/plugins/images/users/d1.jpg')}}" alt="user-img" class="img-circle"> <span class="hide-menu">Dr. Steve Gection<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
                    <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
                    <li><a href="javascript:void(0)" id="logout-button"><i class="fa fa-power-off"></i> Logout</a></li>
                    <form action="/admin-logout" method="post" id="logout-form">{{csrf_field()}}</form>
                </ul>
            </li>
            <li class="nav-small-cap m-t-10">--- Main Menu</li>
            <li> <a href="/dashboard" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard </span></a></li>
            <li><a href="javascript:void(0);" class="waves-effect"><i data-icon=")" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Mailbox<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="/inbox">Inbox</a></li>
                    <li> <a href="/trash-mail">Inbox detail</a></li>
                    <li> <a href="compose.html">Compose mail</a></li>
                </ul>
            </li>
            <li class="nav-small-cap">--- Proffessional</li>
            <li> <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-md p-r-10"></i> <span class="hide-menu"> Admins <span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="/admin-list">All Admins</a> </li>
                    <li> <a href="/create-admin">Create Admin</a> </li>
                </ul>
            </li>
            <li> <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user-md p-r-10"></i> <span class="hide-menu"> Doctors <span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="/doctor-list">All Doctors</a> </li>
                    <li> <a href="/agency-list">All Agency</a> </li>
                    <li><a href="/All Solicitor">All Solicitor</a></li>
                    <li> <a href="/doctor-review">Doctor Review</a> </li>
                    <li> <a href="/agency-review">Agency Review</a> </li>
                    <li> <a href="/solicitor-review">Solicitor Review</a> </li>
                </ul>
            </li>
            <li> <a href="javascript:void(0);" class="waves-effect"><i class="icon-people p-r-10"></i> <span class="hide-menu"> All Appointment <span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li> <a href="patients.html">All Appointments</a> </li>
                    <li> <a href="add-patient.html">Upcoming Appointment</a> </li>
                    <li> <a href="edit-patient.html">Find Apointment</a> </li>
                    <li> <a href="patient-profile.html">Profiles</a> </li>
                </ul>
            </li>
            <li> <a href="/adminfaq" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Faq </span></a></li>
            <li> <a href="/adminnewsletter" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Newsletter </span></a></li>
            <li> <a href="/admincontact" class="waves-effect"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i> <span class="hide-menu"> Contact </span></a></li>
        </ul>
    </div>
</div>
<!-- Left navbar-header end -->