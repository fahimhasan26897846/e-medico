
<!DOCTYPE html>
<html>
<head>
    <title>CREATE NEW ADMIN</title>
    <link href="{{asset('admin/register/css/style.css')}}" rel='stylesheet' type='text/css' />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--webfonts-->
    <link href='http://fonts.googleapis.com/css?family=Lobster|Pacifico:400,700,300|Roboto:400,100,100italic,300,300italic,400italic,500italic,500' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,500,600,700,300' rel='stylesheet' type='text/css'>
    <!--webfonts-->
</head>
<body>
<!--start-login-form-->
<div class="main">
    <div class="login-head">
        <h1>Admin Register</h1>
    </div>
    <div  class="wrap">
        <div class="Regisration">
            <div class="Regisration-head">
                <h2><span></span>Register</h2>
            </div>
            <form action="/postadmin" method="post">
                {{csrf_field()}}
                <input type="text" name="name" placeholder="Name" >
                @if ($errors->has('name'))
                    <p style="color:#e20b0b">{{'*' . $errors->first('name')}}</p>
                @endif
                <input type="text"  name="email" placeholder="Email">
                @if ($errors->has('email'))
                    <p style="color:#e20b0b">{{'*' . $errors->first('email')}}</p>
                @endif
                <input type="text"  name="contact" placeholder="Contact">
                @if ($errors->has('contact'))
                    <p style="color:#e20b0b">{{'*' . $errors->first('contact')}}</p>
                @endif
                <input type="password"  name="password" placeholder="Password">
                @if ($errors->has('password'))
                    <p style="color:#e20b0b">{{'*' . $errors->first('password')}}</p>
                @endif
                <input type="password" name="confirm_password" placeholder="Confirm Password">
                @if ($errors->has('confirm_password'))
                    <p style="color:#e20b0b">{{'*' . $errors->first('confirm_password')}}</p>
                @endif
                <div class="Remember-me">
                    <div class="submit">
                        <input type="submit" onclick="myFunction()" value="Sign Up >" >
                    </div>
                    <div class="clear"> </div>
                </div>

            </form>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    @if(session('success'))
    swal("{{session('success')}}");
    @endif
    @if(session('error'))
    swal("{{session('error')}}");
    @endif
</script>
</body>
</html>


