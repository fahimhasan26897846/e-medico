@extends('admin.inc.master')
@section('title','Dashboard')
@section('body')

    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">DashBoard</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li>Contacts</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <?php $serial = 1 ?>
        <!-- .row -->
        <div class="row bg-title">
            <table class="table table-responsibe table-bordered bg-default table-hover">
                <thead>
                <th>Serial</th>
                <th>Name</th>
                <th>Email</th>
                <th>Contact</th>
                <th>Actions</th>
                </thead>
                <tbody>
                @foreach($contacts as $contact)
                    <tr>
                <th>{{$serial}}</th>
                <th>{{$contact->name}}</th>
                <th>{{$contact->email}}</th>
                        <th>{{$contact->contact}}</th>
                <th>
                    <button class="btn btn-sm btn-rounded btn-info" data-toggle="modal" data-target="#view-{{$contact->id}}"><i class="fa fa-eye"></i>&nbsp;View</button>
                    <div class="modal fade" id="view-{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Message</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        {{$contact->message}}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-sm btn-rounded btn-warning"  data-toggle="modal" data-target="#reply-{{$contact->id}}"><i class="fa fa-reply"></i>&nbsp;Reply</button>
                    <div class="modal fade" id="reply-{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Reply</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="/post-contact-reply" method="post">
                                    {{csrf_field()}}
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="form-group">
                                            <input type="hidden" name="email" value="{{$contact->email}}">
                                            <label for="Subject">Reply</label>
                                            <div class="input-group">
                                                <div class="input-group-addon"><span class="fa fa-user"></span></div>
                                                <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Write Something"></textarea>
                                            </div>
                                            @if ($errors->has('message'))
                                                <p style="color:#e20b0b">{{'*' . $errors->first('message')}}</p>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Reply</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="/delete-contact-message/{{$contact->id}}" class="btn btn-sm btn-rounded btn-danger"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                </th>
                    @php($serial ++)
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- .row -->
    </div>

@endsection

