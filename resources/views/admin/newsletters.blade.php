@extends('admin.inc.master')
@section('title','Dashboard')
@section('body')

    <div class="container-fluid">
        <div class="row bg-title">
            <!-- .page title -->
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">DashBoard</h4>
            </div>
            <!-- /.page title -->
            <!-- .breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="/dashboard">Dashboard</a></li>
                    <li>NewsLetter</li>
                </ol>
            </div>
            <!-- /.breadcrumb -->
        </div>
        <!-- .row -->
        <div class="row">
            <button class="btn btn-sm btn-outline-primary mb-3" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i>&nbsp; Create Newsletter</button>
        </div>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">NewsLetter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/post-new-newsletter" method="post">
                        {{csrf_field()}}
                    <div class="modal-body">
                            <div class="form-group"><label for="Subject">Subject</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><span class="fa fa-pencil"></span></div>
                                    <input type="text" name="subject" placeholder="Subject" class="form-control">
                                </div>
                                @if ($errors->has('subject'))
                                    <p style="color:#e20b0b">{{'*' . $errors->first('subject')}}</p>
                                @endif
                            </div>
                        <div class="form-group"><label for="Subject">Message</label>
                            <div class="input-group">
                                <div class="input-group-addon"><span class="fa fa-user"></span></div>
                                <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Write Something"></textarea>
                            </div>
                            @if ($errors->has('message'))
                                <p style="color:#e20b0b">{{'*' . $errors->first('message')}}</p>
                            @endif

                        </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <?php $serial = 1 ?>
            <table class="table table-responsive table-bordered table-hover bg-secondary">
                <thead>
                <tr>
                    <th scope="col">Serial</th>
                    <th scope="col">Mail</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($emails as $email)
                <tr>
                    <th scope="row">{{$serial}}</th>
                    <td>{{$email->email}}</td>
                    <td><a href="/delete-newsletteremail/{{$email->id}}" class="btn btn-sm btn-rounded btn-danger"><i class="fa fa-trash"></i>&nbsp; DELETE</a></td>

                </tr>
                    <?php $serial++ ?>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- .row -->
    </div>

@endsection

