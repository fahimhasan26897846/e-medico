<?php

/*Visitors*/

Route::get('/','FrontPage\FrontPageController@mainPage');
Route::get('/about','FrontPage\FrontPageController@aboutPage');
Route::get('/contact','FrontPage\FrontPageController@contactPage');
Route::get('/pricing','FrontPage\FrontPageController@pricingPage');
Route::get('/faq','FrontPage\FrontPageController@faqPage');
Route::post('/post-message','FrontPage\ContactController@postMessage');
Route::post('/post-email-newsletter','FrontPage\NewsLetterController@postSubscription');
/*End Visitors*/

/*User*/
Route::get('/show-login','User\UserLogincontroller@showLogin');
Route::get('/show-register','User\UserLogincontroller@showRegister');
Route::post('/postregister','User\UserRegistrationController@postRegistration');
Route::get('/user/{id}/activate/{code}', 'User\UserRegistrationController@activateUser');
Route::post('/postlogin','User\UserLogincontroller@postLogin');
Route::post('/forgetpassword','User\UserLogincontroller@forgotPassword');
Route::get('/user/{id}/reset-password/{code}', 'User\UserLogincontroller@showResetPassword');
Route::put('/user/{id}/reset-password/{code}', 'User\UserLogincontroller@postResetPassword');
Route::group(['middleware'=>'logged_in'],function () {
    Route::get('/userdashboard', 'User\UserLogincontroller@userDashboard');
    Route::post('/logout', 'User\UserLogincontroller@postLogout');
    Route::get('/usersettings', 'User\UserSettingsController@showSettings');
    Route::post('/updategenerelsettings', ['uses' => 'User\UserSettingsController@updateGenerelSettings']);
    Route::post('/updatesettings', ['uses'=>'User\UserSettingsController@updateSettings']);
    Route::post('/updatepersonalinfo', ['uses'=>'User\UserSettingsController@updateInfoSettings']);
    Route::get('/manage-expert/{id}','User\UserController@manageExpert');
    Route::post('/addexpert','User\UserController@addExpert');
    Route::get('/experts/delete/{id}',['uses'=>'User\UserController@deleteExpert']);
    Route::get('/searchslot','User\UserController@searchSlot');
    Route::get('/search-expert','User\UserController@findSlot');
    Route::post('/book-appointment','User\UserController@createAppointment');
    Route::post('/postappointment','User\UserController@postCreateAppointment');
    Route::get('/agency-todays-appointment/{id}','User\UserController@todaysAgencyAppointments');
    Route::get('/agency-weeks-appointment/{id}','User\UserController@thisWeekAgencyAppointments');
    Route::get('/agency-months-appointment/{id}','User\UserController@thisMonthAgencyAppointments');
    Route::get('/experts-todays-appointment/{id}','User\UserController@todaysExpertAppointments');
    Route::get('/experts-weeks-appointment/{id}','User\UserController@thisWeekExpertAppointments');
    Route::get('/experts-months-appointment/{id}','User\UserController@thisMonthExpertAppointments');
    Route::get('/agency-delete/appointment/{id}','User\UserController@deleteAppointment');
    Route::get('/expert-delete/appointment/{id}','User\UserController@deleteAppointment');
    Route::get('/agency-list/{id}','User\UserController@getAgencies');
});

/*Admin*/
Route::get('/admin-login','Admin\AdminLoginController@showLogin');
Route::post('/postadminlogin','Admin\AdminLoginController@postLogin');
Route::group(['middleware'=>'admin'],function (){
    Route::get('/dashboard','Admin\AdminController@showDashboard');
    Route::get('/adminfaq','Admin\AdminController@showFaq');
    Route::post('/admin-postfaq','Admin\AdminController@createFaq');
    Route::post('/admin-updatefaq',['uses'=>'Admin\AdminController@updateFaq']);
    Route::get('/adminnewsletter','Admin\AdminController@showNewsLetter');
    Route::post('/post-new-newsletter','Admin\AdminController@postNewsLetter');
    Route::get('/delete-newsletteremail/{id}','Admin\AdminController@deleteNewsLetter');
    Route::get('/admincontact','Admin\AdminController@adminContact');
    Route::get('/delete-contact-message/{id}','Admin\AdminController@deleteContactMessage');
    Route::post('/post-contact-reply','Admin\AdminController@replyContact');
    Route::post('/admin-logout','Admin\AdminLoginController@postLogout');
    Route::get('/create-admin','Admin\AdminLoginAndRegistrationController@showRegistration');
    Route::post('/postadmin','Admin\AdminLoginAndRegistrationController@postRegistration');
    Route::get('/admin-list','Admin\AdminController@showAdmin');
    Route::get('/deleteadmin/{id}','Admin\AdminController@deleteAdmin');
});
/*End Admin*/

